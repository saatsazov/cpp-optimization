#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstdlib>

using std::cout;
using std::endl;
using std::clock;

void init();
void freeResources();
void runBenchmark(void (*func)(void));

void calcSumm1();
void calcSumm2();

int *arr;

const int ARRAY_SIZE = 10000;

const int RUNS = 100000;

int main()
{
    init();

    cout  << "calcSumm1: ";
    runBenchmark(&calcSumm1);
    cout << endl;

    cout  << "calcSumm2: ";
    runBenchmark(&calcSumm2);
    cout << endl;

    freeResources();

    return 0;
}

void runBenchmark(void (*func)(void))
{
    clock_t startTime = clock();
    for(int i = 0; i < RUNS; i++)
    {
        func();
    }
    cout << float(clock() - startTime) / CLOCKS_PER_SEC ;
}

void init()
{
    srand(time(0));
    arr = new int[ARRAY_SIZE];

    for(int i = 0; i < ARRAY_SIZE;  i++)
    {
        arr[i] = rand();
    }
}

void freeResources()
{
    delete arr;
}

void calcSumm1()
{
    int summ = 0;

    for(int i = 0; i < ARRAY_SIZE; i++)
    {
        summ += arr[i];
    }
}
void calcSumm2()
{
    int summ = 0;

    for(int i = 0; i < ARRAY_SIZE; i += 4)
    {
        summ += arr[i] + arr[i + 1] + arr[i + 2] + arr[i + 3];
    }
}
